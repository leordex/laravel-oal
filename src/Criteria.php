<?php
/**
 * Created by PhpStorm.
 * User: Leordex
 * Date: 29.11.2017
 * Time: 13:48
 */

namespace Leordex\LaravelOAL;

use Illuminate\Support\Arr;
use Leordex\LaravelOAL\Helpers\LogWriter;

class Criteria
{
    private $params = [];

    private $order = [];

    private $deleted = false;

    public function __construct($field = null, $operator = null, $value = null, $boolean = null)
    {
        return $field && $operator !== null ?
            $this->pushParam($field, $operator, $value, $boolean) :
            $this;
    }

    public function withDeleted()
    {
        $this->deleted = true;
        return $this;
    }

    public function pushParam($field, $operator, $value = null, $boolean = null)
    {
        $args = func_get_args();

        if ($value === null) {
            $args[1] = '=';
            $args[2] = $operator;
        }

        if (! $boolean) {
            unset($args[3]);
        }

        $this->params[] = $args;

        return $this;
    }

    public function pushParamPack(array $pack)
    {
        foreach ($pack as $key => $param) {
            try {
                $this->pushParam($key, $param);
            } catch (\Exception $e) {
                LogWriter::onException(
                    trans('laravel-oal::messages.params_import_error'),
                    $e
                );
            }
        }

        return $this;
    }

    public function pushOrder($field, $direction = 'desc')
    {
        $direction = ! in_array($direction, ['desc', 'asc']) ? 'desc' : $direction;
        $this->order[] = [$field, $direction];
        return $this;
    }

    public function extractParams()
    {
        return $this->params;
    }

    public function extractOrder()
    {
        return $this->order;
    }

    public function pullParam($name)
    {
        return ($result = Arr::first($this->params, function ($value) use ($name) {
            return $value[0] === $name;
        }, null)) ? $result[2] : $result;
    }
}