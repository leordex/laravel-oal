<?php

namespace Leordex\LaravelOAL\Tests\Integration;

use Leordex\LaravelOAL\Tests\TestCase;

abstract class TestCaseIntegration extends TestCase
{
    public function setUp()
    {
        parent::setUp();
    }

    public function tearDown()
    {
        parent::tearDown();
    }
}
