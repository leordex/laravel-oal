<?php
/**
 * Created by PhpStorm.
 * User: Leordex
 * Date: 28.11.2017
 * Time: 13:28
 */

namespace Leordex\LaravelOAL\Repositories;

use Leordex\LaravelOAL\Criteria;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

abstract class DatabaseRepo
{
    abstract public function update(
        array $attributes,
        Criteria $criteria,
        array $additionalLoad = []
    ): Collection;

    abstract protected function make(
        Criteria $criteria,
        array $with = [],
        bool $excludeDeleted = true
    ): Builder;

    abstract protected function resetQuery();

    protected function appendSoftDelete(Criteria &$criteria)
    {
        if (! $criteria->pullParam('deleted_at')) {
            $criteria->pushParam('deleted_at', null);
        }
    }

    protected function softDelete(Criteria $criteria) {
        $delete = static::update(
            ['deleted_at' => Carbon::now()->toDateTimeString()],
            $criteria
        );

        return (bool) $delete->count();
    }
}