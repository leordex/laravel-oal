<?php

return [
    'params_import_error' => 'Ошибка импорта параметров',
    'model_not_found' => 'Модель не найдена',
    'stub_not_found' => 'Стаб не найден',
    'model_not_instantiate' => "Экземпляр модели не может быть создан",
    'decorators_location_absent' => "Параметр 'decorators_location' не может быть пустым"
];
