<?php

namespace Leordex\LaravelOAL;

use Leordex\LaravelOAL\Contracts\DatabaseRepoInterface;
use Leordex\LaravelOAL\Repositories\EloquentRepo;
use Illuminate\Support\ServiceProvider;
use Leordex\LaravelOAL\Console\ModelDecoratorsMakeCommand;

class LaravelOALServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/config/oal.php' => config_path('oal.php'),
            ], 'laravel-oal');
            $this->commands([
                ModelDecoratorsMakeCommand::class,
            ]);
        }
        $this->loadTranslationsFrom(
            __DIR__ . '/resources/lang',
            'laravel-oal'
        );

        \Illuminate\Support\Collection::macro('recursive', function () {
            return $this->map(function ($value) {
                if (is_array($value) || is_object($value)) {
                    return collect($value)->recursive();
                }

                return $value;
            });
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/config/oal.php', 'laravel-oal');
        $this->app->singleton(DatabaseRepoInterface::class, EloquentRepo::class);
    }
}
