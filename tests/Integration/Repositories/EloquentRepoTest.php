<?php
/**
 * Created by PhpStorm.
 * User: Leordex
 * Date: 22.06.2018
 * Time: 17:13
 */

namespace Leordex\LaravelOAL\Tests\Integration\Repositories;

use Illuminate\Filesystem\Filesystem;
use Leordex\LaravelOAL\Criteria;
use Leordex\LaravelOAL\Repositories\EloquentRepo;
use Leordex\LaravelOAL\Tests\FakeApp\Database\Seeds\DogBreedsTableSeeder;
use Leordex\LaravelOAL\Tests\FakeApp\Database\Seeds\UsersTableSeeder;
use Leordex\LaravelOAL\Tests\Integration\TestCaseIntegration;

class EloquentRepoTest extends TestCaseIntegration
{
    /** @var Filesystem */
    private $files;

    /** @var EloquentRepo */
    private $userRepo;

    /** @var EloquentRepo */
    private $dogBreedsRepo;

    public function setUp()
    {
        parent::setUp();

        $this->files = app()->make(Filesystem::class);
        try {
            $this->loadMigrationsFrom(
                realpath(
                    __DIR__ . "/../../FakeApp/database/migrations"
                )
            );
        } catch(\Exception $e) {
            echo $e->getMessage();
            exit;
        }

        $this->artisan("make:model-decorator");

        $this->seed(UsersTableSeeder::class);
        $this->seed(DogBreedsTableSeeder::class);

        $this->userRepo = new EloquentRepo();
        $this->userRepo->getModelByDecorator(
            'Leordex\LaravelOAL\Tests\FakeApp\App\Models\User'
        );

        $this->dogBreedsRepo = new EloquentRepo();
        $this->dogBreedsRepo->getModelByDecorator(
            'Leordex\LaravelOAL\Tests\FakeApp\App\Models\DogBreed'
        );
    }

    protected function getEnvironmentSetUp($app)
    {
        parent::getEnvironmentSetUp($app);
        $app->setBasePath(__DIR__ . "/../../FakeApp");
        $app['config']->set('oal.decorators_location', 'Decorators');
        $app['config']->set('oal.models_location', 'Models');
        $app['config']->set(
            'oal.main_orm_model',
            '\Illuminate\Database\Eloquent\Model'
        );
        $app['config']->set('oal.soft_delete_tables', ['users']);
    }

    public function tearDown()
    {
        parent::tearDown();

        $this->files->deleteDirectory(
            __DIR__ . "/../../FakeApp/app/Decorators"
        );
    }

    public function testGet()
    {
        $data = $this->userRepo
            ->get(new Criteria('id', 1));
        $this->assertEquals(1, $data['id']);
        $this->assertEquals('vi', $data['name']);
    }

    public function testGetMany()
    {
        $data = $this->userRepo->getMany(new Criteria);
        $this->assertEquals(1, $data['total']);
        $this->assertCount(1, $data['items']);
        $this->assertEquals(1, $data['items'][0]['id']);
        $this->assertEquals('vi', $data['items'][0]['name']);
    }

    public function testGetCursor()
    {
        $data = $this->userRepo->getCursor(new Criteria);
        $cnt = 0;
        foreach ($data as $item) {
            $this->assertEquals(1, $item->id);
            $this->assertEquals('vi', $item->name);
            $cnt++;
        }
        $this->assertEquals(1, $cnt);
    }

    public function testRestore()
    {
        $criteria = new Criteria('id', 1);
        $this->userRepo->deleteById(1);
        $data = $this->userRepo->get($criteria);
        $this->assertEquals(0, $data->count());

        $this->userRepo->restore($criteria);
        $data = $this->userRepo->get($criteria);
        $this->assertGreaterThan(0, $data->count());
    }

    public function testInsert()
    {
        $this->userRepo->insert(
            [
                [
                    'id' => 2,
                    'name' => 'Atos',
                    'phone' => '+73420340494',
                    'password' => 'white_lily'
                ],
                [
                    'id' => 3,
                    'name' => 'Portos',
                    'phone' => '+73520340494',
                    'password' => 'show_off'
                ]
            ]
        );

        $data = $this->userRepo->getMany(new Criteria);
        $this->assertEquals(3, $data['total']);
        $this->assertCount(3, $data['items']);
        $this->assertEquals('Atos', $data['items'][1]['name']);
        $this->assertEquals('Portos', $data['items'][2]['name']);
    }

    public function testStore()
    {
        $this->userRepo->store([
            'id' => 2,
            'name' => 'Atos',
            'phone' => '+73420340494',
            'password' => 'white_lily'
        ]);

        $data = $this->userRepo->getMany(new Criteria);
        $this->assertEquals(2, $data['total']);
        $this->assertCount(2, $data['items']);
        $this->assertEquals('Atos', $data['items'][1]['name']);
    }

    public function testUpdateById()
    {
        $criteria = new Criteria('id', 1);

        $before = $this->userRepo->get($criteria);
        $this->assertEquals('vi', $before['name']);

        $this->userRepo->updateById([
            'name' => 'pumba'
        ], 1);

        $after = $this->userRepo->get($criteria);
        $this->assertEquals('pumba', $after['name']);
    }

    public function testUpdateMany()
    {
        $this->userRepo->store([
            'id' => 2,
            'name' => 'vi',
            'phone' => '+73420340494',
            'password' => 'white_lily'
        ]);

        $this->userRepo->updateMany(
            ['name' => 'pumba'],
            new Criteria('name', 'vi')
        );

        $data = $this->userRepo->getMany(new Criteria);
        $this->assertEquals('pumba', $data['items'][0]['name']);
        $this->assertEquals('pumba', $data['items'][1]['name']);
    }

    public function testForceDelete()
    {
        $criteria = new Criteria('id', 1);
        $this->userRepo->forceDelete($criteria);

        $result = $this->userRepo->get($criteria);
        $this->assertEquals(0, $result->count());

        $this->userRepo->restore($criteria);

        $result = $this->userRepo->get($criteria);
        $this->assertEquals(0, $result->count());
    }

    public function testDeleteById()
    {
        $criteria = new Criteria('id', 2);

        $data = $this->dogBreedsRepo->get($criteria);
        $this->assertEquals(2, $data['id']);

        $this->dogBreedsRepo->deleteById(2);

        $data = $this->dogBreedsRepo->count($criteria);
        $this->assertEquals(0, $data);
    }

    public function testSync()
    {
        $criteria = new Criteria('id', 1);

        $user = $this->userRepo->get($criteria, ['dogBreeds']);
        $this->assertEquals(0, $user['dog_breeds']->count());

        $this->userRepo->sync(1, 'dogBreeds', [2]);

        $user = $this->userRepo->get($criteria, ['dogBreeds']);
        $this->assertGreaterThan(0, $user['dog_breeds']->count());
        $this->assertEquals(2, $user['dog_breeds'][0]['id']);
    }

    public function testDetach()
    {
        $criteria = new Criteria('id', 1);

        $this->userRepo->sync(1, 'dogBreeds', [2]);

        $user = $this->userRepo->get($criteria, ['dogBreeds']);
        $this->assertGreaterThan(0, $user['dog_breeds']->count());

        $this->userRepo->detach(1, 'dogBreeds', [2]);

        $user = $this->userRepo->get($criteria, ['dogBreeds']);
        $this->assertEquals(0, $user['dog_breeds']->count());
    }

    public function testCount()
    {
        $count = $this->userRepo
            ->count(new Criteria('id', 1));
        $this->assertEquals(1, $count);
    }


}