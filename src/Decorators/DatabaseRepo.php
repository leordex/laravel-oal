<?php
/**
 * Created by PhpStorm.
 * User: Leordex
 * Date: 06.12.2017
 * Time: 15:54
 */

namespace Leordex\LaravelOAL\Decorators;


use Leordex\LaravelOAL\Contracts\DatabaseRepoInterface;
use Leordex\LaravelOAL\Criteria;
use Illuminate\Support\Collection;

abstract class DatabaseRepo implements DatabaseRepoInterface
{
    protected $databaseRepo;

    protected $table;

    public function __construct(DatabaseRepoInterface $databaseRepo)
    {
        $databaseRepo = clone $databaseRepo;
        $this->databaseRepo = $databaseRepo;
        $this->getModelByDecorator(static::class);
    }

    public function get(Criteria $criteria, array $with = []): Collection
    {
        return $this->databaseRepo->get($criteria, $with);
    }

    public function getMany(
        Criteria $criteria,
        array $with = [],
        int $offset = 0,
        int $limit = 1000000
    ): Collection {
        return $this->databaseRepo
            ->getMany($criteria, $with, $offset, $limit);
    }

    public function getCursor(
        Criteria $criteria,
        int $offset = 0,
        int $limit = 1000000
    ) {
        return $this->databaseRepo
            ->getCursor($criteria, $offset, $limit);
    }

    public function save(
        array $attributes,
        Criteria $criteria = null,
        array $additionalLoad = [],
        bool $decide = false
    ): Collection {
        return $this->databaseRepo
            ->save(
                $attributes,
                $criteria,
                $additionalLoad,
                $decide
            );
    }

    public function restore(Criteria $criteria): Collection
    {
        return $this->databaseRepo->restore($criteria);
    }

    public function insert(array $attributes): bool
    {
        return $this->databaseRepo->insert($attributes);
    }

    public function insertIgnore(array $attributes): bool
    {
        return $this->databaseRepo->insertIgnore($attributes);
    }

    public function store(array $attributes, array $additionalLoad = []): Collection
    {
        return $this->databaseRepo->store($attributes, $additionalLoad);
    }

    public function update(
        array $attributes,
        Criteria $criteria,
        array $additionalLoad = []
    ): Collection {
        return $this->databaseRepo
            ->update($attributes, $criteria, $additionalLoad);
    }

    public function updateById(
        array $attributes,
        int $id,
        array $additionalLoad = []
    ): Collection {
        return $this->databaseRepo
            ->updateById($attributes, $id, $additionalLoad);
    }

    public function updateMany(array $attributes, Criteria $criteria): int
    {
        return $this->databaseRepo->updateMany($attributes, $criteria);
    }

    public function forceDelete(Criteria $criteria)
    {
        return $this->databaseRepo->forceDelete($criteria);
    }

    public function delete(Criteria $criteria, bool $force = false)
    {
        return $this->databaseRepo->delete($criteria, $force);
    }

    public function deleteById(int $id)
    {
        return $this->databaseRepo->deleteById($id);
    }

    public function sync(
        int $id,
        string $relation,
        array $data,
        bool $detaching = true
    ): Collection {
        return $this->databaseRepo
            ->sync(
                $id,
                $relation,
                $data,
                $detaching
            );
    }

    public function tripleSync(
        int $id,
        string $relation,
        array $data,
        array $thirdCondition,
        bool $detaching = true
    ): Collection {
        return $this->databaseRepo
            ->tripleSync(
                $id,
                $relation,
                $data,
                $thirdCondition,
                $detaching
            );
    }

    public function detach(
        int $id,
        string $relation,
        array $relationIds
    ) {
        return $this->databaseRepo
            ->detach($id, $relation, $relationIds);
    }

    public function tripleDetach(
        int $id,
        string $relation,
        array $relationIds,
        array $thirdCondition
    ) {
        return $this->databaseRepo
            ->tripleDetach(
                $id,
                $relation,
                $relationIds,
                $thirdCondition
            );
    }

    public function count(Criteria $criteria): int
    {
        return $this->databaseRepo->count($criteria);
    }

    public function getModelByDecorator(string $namespace)
    {
        $this->databaseRepo->getModelByDecorator($namespace);
    }
}