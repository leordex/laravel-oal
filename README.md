# Laravel ORM Abstraction Layer

ORM Abstraction Layer for Laravel

There is support for Eloquent only now. But it can be extended later.

Publish config by command "`php artisan vendor:publish --tag=laravel-oal`" then set `oal.php` in `config` directory according your requirements.

`decorators_location` value can't be empty.

Console command `php artisan make:model-decorator` will generate decorators for all existed models. Also you can generate a decorator for the specific model if you will specify its name after command.

After that you can use decorators instead orm models. You can implement your own methods based on existed in decorators.