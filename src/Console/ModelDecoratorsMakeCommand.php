<?php

namespace Leordex\LaravelOAL\Console;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Leordex\LaravelOAL\Helpers\Common;
use Leordex\LaravelOAL\Helpers\LogWriter;

class ModelDecoratorsMakeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:model-decorator {name?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new decorator class for specified model or for all models';

    /**
     * The filesystem instance.
     *
     * @var Filesystem
     */
    protected $files;

    /**
     * Create a new command instance.
     *
     * @param Filesystem $files
     */
    public function __construct(Filesystem $files)
    {
        parent::__construct();

        $this->files = $files;
    }

    public function handle()
    {
        static::makeDecoratorDirectory();
        $modelsPath = app_path() . "/" . Common::getModelBasePath();
        $modelsNamespace = app()->getNamespace() .
            Common::getModelBaseNamespace();

        if ($name = $this->argument('name')) {
            static::placeDecorator(
                $name,
                $modelsPath,
                $modelsNamespace
            );
            return;
        }

        static::locateModels($modelsPath, $modelsNamespace);
    }

    private function locateModels(string $modelsPath, string $modelsNamespace)
    {
        $models = scandir($modelsPath);
        foreach ($models as $model) {
            if (in_array($model, [".", ".."])) {
                continue;
            }
            if (is_dir($subModelPath = $modelsPath . "/" . $model)) {
                static::locateModels(
                    $subModelPath,
                    $modelsNamespace . "\\" . $model
                );
                continue;
            }
            $modelName = explode(".php", $model, -1);
            if (! $modelName) {
                continue;
            }

            try {
                $model = app()->make(
                    $modelsNamespace . "\\$modelName[0]"
                );
            } catch (\Exception $e) {
                LogWriter::onException(
                    trans('laravel-oal::messages.model_not_instantiate'),
                    $e
                );
                continue;
            };

            $mainModel = config('oal.main_orm_model');
            if (! $model instanceof $mainModel) {
                continue;
            }

            static::placeDecorator(
                $modelName[0],
                $modelsPath,
                $modelsNamespace
            );
        }
    }

    private function placeDecorator(
        string $name,
        string $modelPath,
        string $modelNamespace
    ) {
        $decoratorPath = static::createDecoratorPath(
            $modelPath
        );
        $decoratorNamespace = static::createDecoratorNamespace(
            $modelNamespace
        );

        $this->makeDecoratorDirectory($decoratorPath);

        $file = $decoratorPath . "/" .
            $name . ".php";

        if ($this->files->isFile($file)) {
            return;
        }

        try {
            $stub = $this->files
                ->get(__DIR__ . '/stubs/decorator.stub');
        } catch (\Exception $e) {
            LogWriter::onException(
                trans('laravel-oal::messages.stub_not_found'),
                $e
            );
            return;
        }

        $stub = str_replace(
            [
                'DummyNamespace',
                'DummyClass'
            ],
            [
                $decoratorNamespace,
                $name
            ],
            $stub
        );

        $this->files->put($file, $stub);
    }

    private function createDecoratorPath(string $modelPath): string
    {
        $decoratorPath = substr_replace(
            $modelPath,
            "/" . Common::getDecoratorBasePath(),
            strlen(app_path()),
            0
        );

        if (config('oal.models_location') !== "") {
            $decoratorPath = str_replace(
                "/" . Common::getModelBasePath(),
                "",
                $decoratorPath
            );
        }

        return $decoratorPath;
    }

    private function createDecoratorNamespace(string $modelNamespace): string
    {
        $decoratorNamespace = substr_replace(
            $modelNamespace,
            Common::getDecoratorBaseNamespace(),
            strlen(app()->getNamespace()) - 1,
            0
        );
        if (config('oal.models_location') !== "") {
            $decoratorNamespace = str_replace(
                "\\" . Common::getModelBaseNamespace(),
                "",
                $decoratorNamespace
            );
        }

        return $decoratorNamespace;
    }

    private function makeDecoratorDirectory(string $path = null)
    {
        $dir = $path ?: app_path() . "/" . Common::getDecoratorBasePath();
        if ($this->files->exists($dir)) {
            return;
        }
        $this->files->makeDirectory(
            $dir,
            0777,
            true,
            true
        );
    }
}
