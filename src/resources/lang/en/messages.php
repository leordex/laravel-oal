<?php

return [
    'params_import_error' => 'Parameters import error',
    'model_not_found' => 'Model not found',
    'stub_not_found' => 'Stub not found',
    'model_not_instantiate' => "Model object cannot be created",
    'decorators_location_absent' => "'decorators_location' parameter cannot be empty"
];
