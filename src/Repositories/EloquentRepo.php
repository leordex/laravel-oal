<?php
/**
 * Created by PhpStorm.
 * User: Leordex
 * Date: 27.11.2017
 * Time: 14:43
 */

namespace Leordex\LaravelOAL\Repositories;

use Illuminate\Support\Facades\DB;
use Leordex\LaravelOAL\Contracts\DatabaseRepoInterface;
use Leordex\LaravelOAL\Criteria;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Leordex\LaravelOAL\Helpers\Common;
use Leordex\LaravelOAL\Helpers\LogWriter;

class EloquentRepo extends DatabaseRepo implements DatabaseRepoInterface
{
    private static $specialConditions = [
        'in' => 'whereIn',
        'not_in' => 'whereNotIn',
        'between' => 'whereBetween'
    ];
    private static $nullConditions = [
        'is_null' => 'whereNull',
        'is_not_null' => 'whereNotNull'
    ];
    private static $tripleExtender = 'CustomModel';

    private $table;

    /** @var Model */
    protected $model;

    /** @var Model */
    protected $query;

    public function get(Criteria $criteria, array $with = []): Collection
    {
        return collect($this->make(
            $criteria,
            $with
        )->first())->recursive();
    }

    public function getMany(
        Criteria $criteria,
        array $with = [],
        int $offset = 0,
        int $limit = 1000
    ): Collection {
        $query = $this->make(
            $criteria,
            $with
        );

        $total = $query->count();
        $items = $query->offset($offset)
            ->limit($limit)
            ->get();

        return collect([
            'total' => $total,
            'items' => collect($items->toArray())->recursive()
        ]);
    }

    public function getCursor(
        Criteria $criteria,
        int $offset = 0,
        int $limit = 1000
    ) {
        return $this->make($criteria)
            ->offset($offset)
            ->limit($limit)
            ->cursor();
    }

    public function save(
        array $attributes,
        Criteria $criteria = null,
        array $additionalLoad = [],
        bool $decide = false
    ): Collection {
        if ($criteria instanceof Criteria) {
            $model = $this->make($criteria)
                ->first();

            $model = ! $model && $decide ? $this->model : $model;

            if (! $model) {
                return collect();
            }
        } else {
            $model = $this->model->newModelInstance();
        }

        $model->fill($attributes)
            ->save();

        if ($additionalLoad) {
            $this->appendSoftDeleteToRelations($additionalLoad);
            $model->load(...$additionalLoad);
        }

        return collect($model)->recursive();
    }

    public function restore(Criteria $criteria): Collection
    {
        $model = $this->make($criteria, [], false)
            ->first();

        if (! $model || ! isset($model['deleted_at'])) {
            return collect();
        }

        $model->fill(['deleted_at' => null])
            ->save();

        return collect($model)->recursive();
    }

    public function insert(array $attributes): bool
    {
        $this->resetQuery();
        return $this->rawInsertQuery('insert', $attributes);
    }

    public function insertIgnore(array $attributes): bool
    {
        $this->resetQuery();
        return $this->rawInsertQuery('insert ignore', $attributes);
    }

    public function store(array $attributes, array $additionalLoad = []): Collection
    {
        return $this->save($attributes, null, $additionalLoad);
    }

    public function updateById(
        array $attributes,
        int $id,
        array $additionalLoad = []
    ): Collection {
        return $this->save($attributes, new Criteria('id', $id), $additionalLoad);
    }

    public function update(
        array $attributes,
        Criteria $criteria,
        array $additionalLoad = []
    ): Collection {
        return $this->save($attributes, $criteria, $additionalLoad);
    }

    public function updateMany(array $attributes, Criteria $criteria): int
    {
        return $this->make($criteria)->update($attributes);
    }

    public function forceDelete(Criteria $criteria)
    {
        return $this->delete($criteria, true);
    }

    public function delete(Criteria $criteria, bool $force = false)
    {
        if (
            in_array(
                $this->table,
                config('oal.soft_delete_tables')
            ) && ! $force) {
            return (int) $this->softDelete($criteria);
        }

        $query = $this->make($criteria, [], ! $force);
        $object = $query->first();

        if (! $object) {
            return $object;
        }

        return ! isset($object->id) ?
            $query->delete() :
            $object->delete();
    }

    public function deleteById(int $id)
    {
        return $this->delete(new Criteria('id', $id));
    }

    final public function sync(
        int $id,
        string $relation,
        array $data,
        bool $detaching = true
    ): Collection {
        $result = $this->make(new Criteria('id', $id))
            ->first();

        if (! $result) {
            return collect();
        }

        return collect($result->{$relation}()
            ->sync($data, $detaching));
    }

    final public function tripleSync(
        int $id,
        string $relation,
        array $data,
        array $thirdCondition,
        bool $detaching = true
    ): Collection {
        if (! $this->isTripleAllowed()) {
            abort(500, trans('laravel-oal::messages.relation_not_allowed_triple'));
        }

        $result = $this->make(new Criteria('id', $id))
            ->first();

        if (! $result) {
            return collect();
        }

        return collect($result->{$relation}()
            ->sync($data, $detaching, $thirdCondition));
    }

    final public function detach(
        int $id,
        string $relation,
        array $relationIds
    ) {
        $result = $this->make(new Criteria('id', $id))
            ->first();

        if (! $result) {
            return collect();
        }

        return collect($result->{$relation}()
            ->detach($relationIds));
    }

    final public function tripleDetach(
        int $id,
        string $relation,
        array $relationIds,
        array $thirdCondition
    ) {
        if (! $this->isTripleAllowed()) {
            abort(500, trans('laravel-oal::messages.relation_not_allowed_triple'));
        }

        $result = $this->make(new Criteria('id', $id))
            ->first();

        if (! $result) {
            return collect();
        }

        return collect($result->{$relation}()
            ->detach($relationIds, $thirdCondition));
    }

    final public function count(Criteria $criteria): int
    {
        return $this->make(
            $criteria
        )->count();
    }

    public function getModelByDecorator(string $namespace)
    {
        try {
            $this->model = app()->make(
                str_replace(
                    "\\" . Common::getDecoratorBaseNamespace(),
                    Common::getModelBaseNamespace() ?
                        "\\" . Common::getModelBaseNamespace() : "",
                    $namespace
                )
            );
        } catch (\Exception $e) {
            LogWriter::onException(
                trans('laravel-oal::messages.model_not_instantiate'),
                $e
            );
        }

        if (! $this->model instanceof Model) {
            abort(500, trans('laravel-oal::messages.model_not_found'));
        }
        $this->table = $this->model->getTable();
    }

    /**
     * @param Criteria $criteria
     * @param array $with
     * @param bool $excludeDeleted
     * @return Builder
     */
    final protected function make(
        Criteria $criteria,
        array $with = [],
        bool $excludeDeleted = true
    ): Builder {
        $this->resetQuery();

        if ($excludeDeleted) {
            if (in_array($this->table, config('oal.soft_delete_tables'))) {
                $criteria = clone($criteria);
                $this->appendSoftDelete($criteria);
            }
            $this->appendSoftDeleteToRelations($with);
        }

        $this->query = $this->query->with($with);

        $conditions = $criteria->extractParams();

        $order = $criteria->extractOrder();

        foreach ($conditions as $condition) {
            if (
                in_array(
                    $condition[1],
                    array_keys(static::$specialConditions),
                    true
                ) &&
                is_array($condition[2])
            ) {
                $specialCondition = static::$specialConditions[$condition[1]];
                unset($condition[1]);
                $this->query->{$specialCondition}(...array_values($condition));
                continue;
            }

            if (
                in_array(
                    $condition[2],
                    array_keys(static::$nullConditions),
                    true
                )
            ) {
                $nullCondition = static::$nullConditions[$condition[2]];
                unset($condition[1], $condition[2]);
                $this->query->{$nullCondition}(...array_values($condition));
                continue;
            }
            $this->query->where(...$condition);
        }

        foreach ($order as $orderItem) {
            $this->query->orderBy(...$orderItem);
        }

        return $this->query;
    }

    protected function resetQuery()
    {
        $this->query = $this->model->newQueryWithoutRelationships();
    }

    private function rawInsertQuery($command, array $attributes)
    {
        $this->resetQuery();

        if (! count($attributes)) {
            return true;
        }

        if (is_array(array_values($attributes)[0])) {
            $attributes = array_map(function($value) {
                ksort($value);
                return $value;
            }, $attributes);
        } else {
            $attributes = [$attributes];
        }

        $keys = array_map(function($key) {
            return "`{$key}`";
        }, array_keys($attributes[0]));

        $inserts =
        $bindings = [];

        $query = $command . " into " .
            DB::connection(
                $this->model->getConnectionName()
            )->getTablePrefix() .
            $this->model->getTable() .
            " (" . implode(",", $keys) . ") values ";

        foreach($attributes as $data) {
            $qs = [];
            foreach($data as $value) {
                $qs[] = '?';
                $bindings[] = $value;
            }
            $inserts[] = '('.implode(",", $qs).')';
        }
        $query .= implode(",", $inserts);

        return DB::connection($this->model->getConnectionName())
            ->insert($query, $bindings);
    }

    private function isTripleAllowed()
    {
        try {
            $reflection = new \ReflectionClass($this->model);
        } catch (\Exception $e) {
            abort(500, trans('laravel-oal::messages.model_not_found'));
        }

        return ! (
            strpos(
                $reflection->getParentClass()->name,
                static::$tripleExtender
            ) === false
        );
    }

    private function appendSoftDeleteToRelations(array &$relations)
    {
        $updatedRelations = [];
        foreach ($relations as $relation) {
            $query = $this->model;
            $relationParts = explode('.', $relation);
            $relationCnt = count($relationParts);
            foreach($relationParts as $key => $relationPart) {
                if ($key - 1 === $relationCnt) {
                    $query = $query->{$relationPart}();
                    break;
                }
                $query = $query->{$relationPart}()->getModel();
            }

            $table = $query->getTable();
            if (in_array($table, config('oal.soft_delete_tables'))) {
                $updatedRelations[$relation] = function($query) {
                    return $query->whereNull('deleted_at');
                };
                continue;
            }
            $updatedRelations[] = $relation;
        }
        $relations = $updatedRelations;
    }
}