<?php

namespace Leordex\LaravelOAL\Tests\FakeApp\App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $fillable = [
        'id',
        'name',
        'phone',
        'password',
        'deleted_at'
    ];

    public function dogBreeds()
    {
        return $this->belongsToMany(DogBreed::class);
    }
}
