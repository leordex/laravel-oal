<?php
/**
 * Created by PhpStorm.
 * User: Leordex
 * Date: 27.11.2017
 * Time: 16:53
 */

namespace Leordex\LaravelOAL\Contracts;


use Leordex\LaravelOAL\Criteria;
use Illuminate\Support\Collection;

interface DatabaseRepoInterface
{
    public function get(Criteria $criteria, array $with = []): Collection;

    public function getMany(
        Criteria $criteria,
        array $with = [],
        int $offset = 0,
        int $limit = 1000
    ): Collection;

    public function getCursor(
        Criteria $criteria,
        int $offset = 0,
        int $limit = 1000
    );

    public function save(
        array $attributes,
        Criteria $criteria = null,
        array $additionalLoad = [],
        bool $decide = false
    ): Collection;

    public function restore(Criteria $criteria): Collection;

    public function insert(array $attributes): bool;

    public function insertIgnore(array $attributes): bool;

    public function store(array $attributes, array $additionalLoad = []): Collection;

    public function update(
        array $attributes,
        Criteria $criteria,
        array $additionalLoad = []
    ): Collection;

    public function updateById(
        array $attributes,
        int $id,
        array $additionalLoad = []
    ): Collection;

    public function updateMany(array $attributes, Criteria $criteria): int;

    public function forceDelete(Criteria $criteria);

    public function delete(Criteria $criteria, bool $force = false);

    public function deleteById(int $id);

    public function sync(
        int $id,
        string $relation,
        array $data,
        bool $detaching = true
    ): Collection;

    public function tripleSync(
        int $id,
        string $relation,
        array $data,
        array $thirdCondition,
        bool $detaching = true
    ): Collection;

    public function detach(
        int $id,
        string $relation,
        array $relationIds
    );

    public function tripleDetach(
        int $id,
        string $relation,
        array $relationIds,
        array $thirdCondition
    );

    public function count(Criteria $criteria): int;

    public function getModelByDecorator(string $namespace);
}