<?php
/**
 * Created by PhpStorm.
 * User: Leordex
 * Date: 22.06.2018
 * Time: 17:13
 */

namespace Leordex\LaravelOAL\Tests\Integration\Console;

use Illuminate\Filesystem\Filesystem;
use Leordex\LaravelOAL\Tests\Integration\TestCaseIntegration;

class ModelDecoratorsMakeCommandTest extends TestCaseIntegration
{
    /** @var Filesystem */
    private $files;

    public function setUp()
    {
        parent::setUp();
        $this->files = app()->make(Filesystem::class);
    }

    protected function getEnvironmentSetUp($app)
    {
        $app->setBasePath(__DIR__ . "/../../FakeApp");
        $app['config']->set('oal.decorators_location', 'Decorators');
        $app['config']->set('oal.models_location', 'Models');
        $app['config']->set('oal.main_orm_model', '\Illuminate\Database\Eloquent\Model');
    }

    public function testHandle()
    {
        $userFile =
<<<CODE
<?php

namespace Leordex\LaravelOAL\Tests\FakeApp\AppDecorators;

use Leordex\LaravelOAL\Decorators\DatabaseRepo;

class User extends DatabaseRepo
{
    //
}
CODE;

        $fileFile =
<<<CODE
<?php

namespace Leordex\LaravelOAL\Tests\FakeApp\AppDecorators\User;

use Leordex\LaravelOAL\Decorators\DatabaseRepo;

class File extends DatabaseRepo
{
    //
}
CODE;
        $decoratorsDir = __DIR__ . "/../../FakeApp/app/Decorators";
        $this->assertFalse(
            $this->files
                ->exists($decoratorsDir)
        );
        $this->artisan(
            "make:model-decorator",
            ['name' => 'User']
        );
        $this->assertTrue(
            $this->files
                ->exists($decoratorsDir)
        );
        $this->assertTrue(
            $this->files
                ->exists($decoratorsDir . "/User.php")
        );
        $this->assertGreaterThan(
            0,
            $this->files
                ->size($decoratorsDir . "/User.php")
        );


        $this->artisan("make:model-decorator");
        $this->assertTrue(
            $this->files
                ->exists($decoratorsDir . "/User/File.php")
        );
        $this->assertGreaterThan(
            0,
            $this->files
                ->size($decoratorsDir . "/User/File.php")
        );

        $this->assertCount(
            5,
            scandir($decoratorsDir)
        );
        $this->assertCount(
            3,
            scandir($decoratorsDir . "/User")
        );
        $this->assertEquals($userFile, $this->files->get($decoratorsDir . "/User.php"));
        $this->assertEquals($fileFile, $this->files->get($decoratorsDir . "/User/File.php"));

        $this->files->deleteDirectory($decoratorsDir);
    }
}