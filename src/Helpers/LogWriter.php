<?php
/**
 * Created by PhpStorm.
 * User: Leordex
 * Date: 19.12.2017
 * Time: 14:32
 */

namespace Leordex\LaravelOAL\Helpers;

use Illuminate\Support\Facades\Log;

class LogWriter
{
    public static function onException($error, \Exception $e, $trace = false)
    {
        $data = [
            'message' => $e->getMessage(),
            'errorCode' => $e->getCode(),
            'line' => $e->getFile() . " " . $e->getLine()
        ];

        Log::error($error, $data);
    }
}