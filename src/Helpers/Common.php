<?php
/**
 * Created by PhpStorm.
 * User: Leordex
 * Date: 19.12.2017
 * Time: 14:32
 */

namespace Leordex\LaravelOAL\Helpers;

class Common
{
    private static $decoratorsBasePath = "Decorators";

    public static function getDecoratorBaseNamespace()
    {
        if (! config('oal.decorators_location')) {
            abort(
                500,
                trans('laravel-oal::messages.decorators_location_absent')
            );
        }
        return str_replace(
            "/",
            "\\",
            trim(
                config('oal.decorators_location'),
                "/\\"
            )
        );
    }

    public static function getModelBaseNamespace()
    {
        return str_replace(
            "/",
            "\\",
            trim(
                config('oal.models_location'),
                "/\\"
            )
        );
    }

    public static function getDecoratorBasePath()
    {
        if (! config('oal.decorators_location')) {
            abort(
                500,
                trans('laravel-oal::messages.decorators_location_absent')
            );
        }
        return str_replace(
            "\\",
            "/",
            trim(
                config('oal.decorators_location'),
                "/\\"
            )
        );
    }

    public static function getModelBasePath()
    {
        return str_replace(
            "\\",
            "/",
            trim(
                config('oal.models_location'),
                "/\\"
            )
        );
    }
}