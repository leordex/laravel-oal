<?php

return [
    'decorators_location' => 'Decorators',
    'models_location' => "",
    'soft_delete_tables' => [],
    'main_orm_model' => '\Illuminate\Database\Eloquent\Model'
];
